//creating table of matches//
create table match_data ( id int, season int, city varchar(20), date varchar(20), team1 varchar(70), team2 varchar(70), toss_winner varchar(70), toss_decision varchar(20), result varchar(20), dl_applied int, winner varchar(70), win_by_runs int, win_by_wickets int, player_of_match varchar(70), venue varchar(100), umpire1 varchar(100), umpire2 varchar(100), umpire3 varchar(100) default 'null', primary key (id));

//creating table of deliveries//
create table delivery_data (match_id int, inning int, batting_team varchar(100), bowling_team varchar(100),
overr int, ball int, batsman varchar(100), non_striker varchar(100), bowler varchar(100),
is_super_over int, wide_runs int, bye_runs int, legbye_runs int, noball_runs int, penalty_runs int,
batsman_runs int, extra_runs int, total_runs int, player_dismissed varchar(100), dismissal_kind varchar(100),
fielder varchar(100), foreign key(match_id) references match_data(id));

///Question 1: Number of matches played per year of all the years in IPL///
select season,count(id) from match_data group by season order by season;

///Question 2: Number of matches won of all teams over all the years of IPL//
select winner, count(winner) from match_data group by winner order by winner;

///Question 3: For the year 2016 get the extra runs conceded per team///
select season,bowling_team, sum(extra_runs) from delivery_data
left join match_data
on delivery_data.match_id = match_data.id 
where season='2016'
group by bowling_team;

///Question 4: For the year 2015 get the top economical bowlers///
select bowler, ((sum(total_runs)/count(bowler))*6) as 'economy' from delivery_data join
match_data on delivery_data.match_id = match_data.id
where season='2015'
group by bowler order by economy;
